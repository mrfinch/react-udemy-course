import React, { Component } from 'react';
import './App.css';
import Radium from 'radium'
import Persons from '../components/Persons/Persons'
import Cockpit from '../components/Cockpit/Cockpit'

class App extends Component {

  state = {
    persons: [
      {name: "Saurabh", id: 1},
      {name: "Abc", id: 2}
    ],
    showPersons: false,
    toggleCount: 0
  }

  componentWillMount = () => {
    console.log("App -> willMount")
  }

  componentDidMount = () => {
    console.log("App -> didMount")
  }

  shouldComponentUpdate(nextProps, nextState) {
    console.log("App -> shouldCompUpdate", nextProps, nextState)
    return true;
  }

  componentWillUpdate(nextProps, nextState) {
      console.log("App -> compWillUpdate", nextProps, nextState)
  }

  componentDidUpdate() {
      console.log("App -> compDidUpdate")
  }

  switchNameHandler = (index) => {
    this.state.persons[index].name += Math.floor(Math.random() * 20); 
    this.setState({
      persons: this.state.persons
    });
  }

  newNameHandler = (event, id) => {
    const copiedPersons = [...this.state.persons]
    const p = this.state.persons.find(person => person.id === id)
    p.name = event.target.value;
    this.setState({
      persons: copiedPersons
    });
  }

  togglePersonsHandler = () => {
    this.setState( (prevState, props) => {
      return {
        showPersons: !prevState.showPersons,
        toggleCount: prevState.toggleCount + 1
      }
    })
  }

  deleteHandler = (index) => {
    // const p = this.state.persons.slice();
    const p = [...this.state.persons]
    p.splice(index, 1)
    this.setState({persons: p})
  }

  render() {
    console.log('App -> render')
    let persons = null;
    if(this.state.showPersons) {
      persons = (
        <div>
          <Persons 
          clicked={this.deleteHandler} 
          changed={this.newNameHandler} 
          persons={this.state.persons}
          />
        </div>
      )
    }
    
    return (
      <div className="App">
        <Cockpit title={this.props.title} clicked={this.togglePersonsHandler} showPersons={this.state.showPersons} 
        persons={this.state.persons}
        />
        {persons}
      </div>
    );
  }
}

export default Radium(App);
