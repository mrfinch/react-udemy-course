import React, {Component} from 'react'
import Person from './Person/Person'

class Persons extends Component {
    componentWillMount = () => {
        console.log("Persons -> willMount")
    }
    
    componentDidMount = () => {
        console.log("Persons -> didMount")
    }

    componentWillReceiveProps (nextProps) {
        console.log("Persons -> willReceive Props", nextProps)
    }

    shouldComponentUpdate(nextProps, nextState) {
        console.log("Persons -> shouldCompUpdate", nextProps, nextState)
        return true;
    }

    componentWillUpdate(nextProps, nextState) {
        console.log("Persons -> compWillUpdate", nextProps, nextState)
    }

    componentDidUpdate() {
        console.log("Persons -> compDidUpdate")
    }

    render() {
        console.log("Persons -> render")
        return (
            this.props.persons.map((person, ind) => {
                return <Person name={person.name} delete={() => this.props.clicked(ind)} key={person.id} changed={(e) => this.props.changed(e, person.id)} />
            })
        );
    }
}

export default Persons;