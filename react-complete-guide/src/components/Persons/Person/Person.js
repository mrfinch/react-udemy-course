import React, {Component} from 'react'
import './Person.css'
import PropTypes from 'prop-types'

class Person extends Component {
    render() {
        const style = {
            backgroundColor: 'yellow'
        }
        
        return (
            <div className="Person" style={style}>
                <p>Name: {this.props.name} <input type="text" onChange={this.props.changed} 
                value={this.props.name}/>
                <button onClick={this.props.delete}>Delete Name</button>
                </p>
            </div> 
        );
         
    }
}

Person.propTypes = {
    name: PropTypes.string,
    changed: PropTypes.func,
    delete: PropTypes.func
};

export default Person;
