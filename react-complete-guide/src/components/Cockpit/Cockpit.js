import React from 'react'
import './Cockpit.css'
import Aux from '../../hoc/Aux'

const cockpit = (props) => {
    const classes = []
    if (props.persons.length > 1) {
      classes.push('bold');
    }
    if (props.persons.length > 0) {
      classes.push('red');
    }

    const btnStyle = {
        backgroundColor: 'green',
        ':hover': {
          backgroundColor: 'lightblue'
        }
    }
    if (props.showPersons) {
        btnStyle.backgroundColor = 'red';
        btnStyle[':hover'] = {
            backgroundColor: 'yellow'
        }
    }
      

    return (
        <Aux>
            <h1>{props.title}</h1>
            <p className={classes.join(' ')}>Styles working</p>
            <button onClick={props.clicked} style={btnStyle}>Toggle Persons</button>
        </Aux>
    )
}

export default cockpit;