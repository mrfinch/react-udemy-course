import * as actionTypes from '../actions';

const initialState = {
    results: []
}

const result = (state = initialState, action) => {
    if (action.type === actionTypes.STORE_RESULT) {
        return {
            ...state,
            results: state.results.concat({id: new Date(),value: action.result})
        }
    } else if (action.type === actionTypes.DELETE_RESULT) {
        return {
            ...state,
            results: state.results.filter((result) => {
                return result.id !== action.id;
            })
        }
    }
    return state;
}

export default result;
